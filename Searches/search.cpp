#include <iostream>

using namespace std;

int linearSearch(int arr[], int element, int size);
int binarySearch(int arr[], int element, int size);

int main()
{
	int searchArray[] = {9,4,6,3,2,8,7,1,5};
	int searchArrayBinary[] = {1,2,3,4,5,6,7,8};
	int sizeLinear = (sizeof(searchArray)/sizeof(searchArray[0]));
	int sizeBinary = (sizeof(searchArrayBinary)/sizeof(searchArrayBinary[0]));
	linearSearch(searchArray,7,sizeLinear);
	binarySearch(searchArrayBinary,7,sizeBinary);
	cin.get();
	return 0;
}

int linearSearch(int arr[],int element,int size)
{
	for(int i=0;i<size;i++)
	{
		if(arr[i]==element)
		{
			cout<<"Element "<<element<<" found at position "<<i<<endl;
			return i;
		}
	}

	cout<<"Element "<<element<<" not found";
	return -1;
}

int binarySearch(int arr[], int element, int size)
{
	int front =0,end = size-1,mid = (front+end)/2;
	while(front <= end)
	{
		if(arr[mid]==element)
		{
			cout<<"Element "<<element<<" found at position "<<mid<<endl;
			return mid;
		}
		if(arr[mid]<element)
		{
			front = mid+1;
			mid = (front+end)/2;
			continue;
		}
		if(arr[mid]>element)
		{
			end = mid-1;
			mid = (front+end)/2;
			continue;
		}
	}
	cout<<"Element "<<element<<" not found";
	return -1;
}
